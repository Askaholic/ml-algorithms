import argparse
import csv
import random
import sys
from collections import defaultdict
from typing import Dict, Iterator, List, Tuple

Data = Dict[str, List[float]]
DataPoint = Dict[str, float]

BIAS = '__BIAS'


class LinearRegressionModel(object):
    def __init__(self, data: Data, target_feature: str, alpha=0.00000002):
        self.weights: Dict[str, float] = defaultdict(
            lambda: random.uniform(0, 1)
        )
        self.data = data
        self.data_rows = transpose_data(data)
        self.target_feature = target_feature
        self.alpha = alpha

    def input_features(self, data: DataPoint):
        return filter(lambda kv: kv[0] != self.target_feature, data.items())

    def predict(self, data_point: DataPoint):
        return self.weights[BIAS] + sum(
            self.weights[k] * v for k, v in data_point.items()
        )

    def predict_many(self, data_point_iter: Iterator[DataPoint]):
        return (self.predict(data_point) for data_point in data_point_iter)

    def train(self, iters=100_000) -> None:
        alpha = self.alpha
        for i in range(iters):
            predicted = list(self.predict_many(
                map(
                    lambda x: {
                        k: v for k, v in self.input_features(x)
                    },
                    self.data_rows
                )
            ))

            error = list(t - mt for t, mt in zip(self.data[self.target_feature], predicted))
            weight_errors = {
                k: sum(map(lambda x: x * self.weights[k], error))
                for k in filter(lambda k: k != self.target_feature, self.data)
            }

            total_sq_error = sum(map(lambda x: x * x, error))
            print("Total sq error: {:.2f} alpha: {:.8f} completion: {:.2f}%".format(
                total_sq_error, alpha, i/iters * 100
            ), end='\r')
            sys.stdout.flush()
            if i % (iters/10) == 0:
                print()
            weight_errors[BIAS] = sum(error)
            for k, err in weight_errors.items():
                self.weights[k] += alpha * err
        print()


def transpose_data(data: Data) -> List[DataPoint]:
    transposed: List[DataPoint] = []
    for k, v in data.items():
        for i, point in enumerate(v):
            if i >= len(transposed):
                transposed.append({})
            transposed[i][k] = point
    return transposed


def read_data_from_csv(file_path: str) -> Tuple[Data, str]:
    """
    Expects csv file to look like:
    header1,header2,header3
    data11, data21, data31
    data12, data22, data32
    data13, data23, data33
    """
    with open(file_path) as f:
        reader = csv.reader(f)

        headers = next(reader)
        data: Data = defaultdict(list)

        for row in reader:
            assert len(headers) == len(row)
            for k, v in zip(headers, row):
                data[k].append(float(v))
        return data, headers[-1]


def main(file_path: str, training_iters: int):
    data, target_feature = read_data_from_csv(file_path)

    model = LinearRegressionModel(data, target_feature, alpha=0.0000002)
    model.train(iters=training_iters)

    print("Model weights: ", model.weights)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("FILE_PATH", help="Path to the input data in CSV format")
    parser.add_argument("ITERATIONS", type=int, help="Number of iterations to perform training")

    args = parser.parse_args()
    main(args.FILE_PATH, args.ITERATIONS)
